from setuptools import setup, find_packages
from distutils.util import convert_path
from os import path

ns = {}
ver_path = convert_path('vinfra/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), ns)
version = ns['__version__']

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='vinfra',
    packages=find_packages(),
    python_requires='>=3.9',
    url='https://gitlab.com/alessio-linares/virtual-infrastructure/vinfra-cli',
    project_urls={
        'Source': 'https://gitlab.com/alessio-linares/virtual-infrastructure/vinfra-cli',
    },
    description='CLI app for Alessio Virtual Infrastructure',
    long_description=long_description,
    long_description_content_type='text/markdown',
    license='Apache 2.0',
    author='Alessio Linares',
    author_email='mail@alessio.cc',
    version=version,
    entry_points={
        'console_scripts': [
            'vinfra = vinfra.__main__:entrypoint',
        ],
    },
    data_files=[],
    scripts=[],
    install_requires=[
        'PyYAML==5.3.1',
        'requests==2.25.0',
        'argcomplete>=1.12.2,<2.0.0',
        'applipy>=2.1.0,<3.0.0',
        'applipy-inject>=1.2.0,<2.0.0',
    ],
)
