# Alessio Virtual Infrastructure CLI

This programs lets you interact with your virtual infrastructure.

```bash
pip install vinfra --extra-index-url https://gitlab.com/api/v4/projects/22750752/packages/pypi/simple
```
