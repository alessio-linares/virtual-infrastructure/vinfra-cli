FROM python:3.12.3-alpine

LABEL maintainer="mail@alessio.cc"
LABEL website="vinfra.alessio.cc"
LABEL source="https://gitlab.com/alessio-linares/virtual-infrastructure/vinfra-cli/"

COPY dist/vinfra-*.whl ./
RUN pip install vinfra-*.whl
RUN rm -f vinfra-*.whl

ENTRYPOINT ["vinfra"]
CMD ["--help"]
