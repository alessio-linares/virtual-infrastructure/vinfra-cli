from applipy import Application, Config, LoggingModule
from .modules.config import ConfigModule
from .modules.resource import ResourceModule
from .modules.service import ServiceModule
from .app import VinfraCli, PROG
from vinfra.exit_code import ExitCode


def entrypoint():
    app = (
        Application(
            Config({"app.name": PROG, "logging.level": "ERROR"}),
            shutdown_timeout_seconds=0.01,
        )
        .register(VinfraCli)
        .install(ConfigModule)
        .install(ResourceModule)
        .install(ServiceModule)
    )
    exit_code = ExitCode(app)
    app.injector.bind(ExitCode, exit_code)
    app.run()
    exit(exit_code.exit_code)


if __name__ == "__main__":
    entrypoint()
