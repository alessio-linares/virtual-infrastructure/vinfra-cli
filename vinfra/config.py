import os

import yaml


RESOURCE_TYPES = ('service', 'secret', 'volume', 'network', 'cronjob')


CONFIG_DIR = os.path.join(os.path.expanduser('~'), '.config', 'vinfra')
CONFIG_FILE = os.path.join(CONFIG_DIR, 'config')


class Config:

    def __init__(self):
        self._load()

    def _load(self):
        self._data = None
        os.makedirs(CONFIG_DIR, exist_ok=True)
        if os.path.exists(CONFIG_FILE):
            with open(CONFIG_FILE, 'rb') as f:
                self._data = yaml.safe_load(f)

        if self._data is None:
            self._data = {}

    def _save(self):
        os.makedirs(CONFIG_DIR, exist_ok=True)
        with open(CONFIG_FILE, 'w') as f:
            yaml.safe_dump(self._data, f)

    @property
    def active_identity(self):
        return self._data.get('active_identity')

    @active_identity.setter
    def active_identity(self, value):
        self._data['active_identity'] = value
        self._save()

    def get_token(self, identity):
        return self._data['identities'][identity]

    def add_identity(self, identity, token):
        if 'identities' not in self._data:
            self._data['identities'] = {}

        if identity in self._data['identities']:
            raise ValueError('identity already exists')
        self._data['identities'][identity] = token
        self._save()

    def set_identity(self, identity, token):
        self._data['identities'][identity] = token
        self._save()

    def remove_identity(self, identity):
        del self._data['identities'][identity]
        if not self._data['identities']:
            del self._data['identities']

        self._save()

    def list_identities(self):
        if 'identities' not in self._data:
            return []

        return list(self._data['identities'].keys())

    @property
    def editor(self):
        return self._data.get('editor') or os.environ.get('EDITOR')

    @editor.setter
    def editor(self, value):
        self._data['editor'] = value
        self._save()

    def list_known_accounts(self):
        if 'known_accounts' not in self._data:
            return []
        return self._data['known_accounts']

    def add_known_account(self, account):
        account = str(account)
        if 'known_accounts' not in self._data:
            self._data['known_accounts'] = []
        if account in self._data['known_accounts']:
            return
        self._data['known_accounts'].append(account)
        self._save()


def get_token(args, config):
    if args.token:
        return args.token

    identity = args.identity or config.active_identity
    if not args.token and not identity:
        print('Missing identity or token. Either set your active identity with '
              '`vinfra config identity active <name>` or use the `--token` option')
        exit(1)

    try:
        return config.get_token(identity)
    except KeyError:
        print('Identity not found in local store. You may add it with `vinfra config identity add <name>`')
        exit(1)
