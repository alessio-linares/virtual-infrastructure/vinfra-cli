import requests

from vinfra.config import (
    Config,
    RESOURCE_TYPES,
    get_token,
)


class Completer:

    def __call__(self, prefix, parsed_args, **kwargs):
        raise NotImplementedError()


class AccountCompleter(Completer):

    def __init__(self, config: Config):
        self.config = config

    def __call__(self, prefix, parsed_args, **kwargs):
        return self.config.list_known_accounts()


class ResourceTypeCompleter(Completer):

    def __call__(self, prefix, parsed_args, **kwargs):
        return RESOURCE_TYPES


class ResourceIdCompleter(Completer):

    def __init__(self, config: Config):
        self.config = config

    def __call__(self, prefix, parsed_args, **kwargs):
        splits = prefix.split('-', 2)

        if len(splits) <= 2:
            return [a + '-' + t for a in self.config.list_known_accounts() for t in RESOURCE_TYPES]

        token = get_token(parsed_args, self.config)
        url = f'{parsed_args.api_endpoint}/v1/resources/{splits[0]}/{splits[1]}'
        response = requests.get(url, headers={'Authorization': f'Bearer {token}'})
        if not response.ok:
            return ()
        return response.json()
