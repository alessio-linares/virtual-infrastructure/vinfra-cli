from applipy import Module

from .completers import (
    Completer,
    AccountCompleter,
    ResourceIdCompleter,
    ResourceTypeCompleter,
)

from .config import Config


class CommonModule(Module):

    def configure(self, bind, register):
        bind((Completer, AccountCompleter), AccountCompleter)
        bind((Completer, ResourceIdCompleter), ResourceIdCompleter)
        bind((Completer, ResourceTypeCompleter), ResourceTypeCompleter)
        bind(Config)
