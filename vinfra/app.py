import json
from argparse import ArgumentParser
from enum import Enum
from typing import List

import yaml
from applipy import AppHandle

from .config import Config, get_token
from .modules.base import CliModule
from .version import __version__
from vinfra.exit_code import ExitCode


try:
    from argcomplete import autocomplete
except ImportError:

    def autocomplete(*args):
        ...


class Format(str, Enum):
    YAML = "yaml"
    JSON = "json"


formatter_mapping = {
    Format.YAML.value: lambda data, file: yaml.dump(data, file, sort_keys=True),
    Format.JSON.value: lambda data, file: json.dump(
        data, file, indent=2, sort_keys=True
    ),
}


PROG = "vinfra-cli"


class VinfraCli(AppHandle):
    def __init__(self, modules: List[CliModule], config: Config, exit_code: ExitCode):
        self._modules = modules
        self._config = config
        self._exit_code = exit_code

    async def on_start(self):
        try:
            parser = ArgumentParser(
                prog=PROG,
                description="Alessio Virtual Infrastructure."
                " This programs lets you interact with your virtual infrastructure.",
            )
            parser.add_argument(
                "--api-endpoint",
                type=str,
                required=False,
                default="https://api.vinfra.alessio.cc",
                help="URL pointing at where the VInfra API is located",
            )
            parser.add_argument(
                "--identity",
                type=str,
                default=None,
                choices=self._config.list_identities(),
                metavar="IDENTITY",
                help="locally stored identity to authenticate you",
            )
            parser.add_argument(
                "--token",
                type=str,
                default=None,
                help="vinfra API token to authenticate you",
            )
            parser.add_argument(
                "--format",
                choices=formatter_mapping.keys(),
                help="Format of the output",
                default=Format.YAML.value,
            )
            parser.add_argument(
                "-v", "--version", help="Show version string", action="store_true"
            )
            parser.set_defaults(requires_token=False)
            parser.set_defaults(func=None)

            subparsers = parser.add_subparsers()
            subparsers.dest = "module"
            for m in self._modules:
                m.register_parser(subparsers)

            autocomplete(parser)
            args = parser.parse_args()
            args.write_formatted = formatter_mapping[args.format]

            if args.version:
                print(PROG, __version__)
                exit(0)
            if args.requires_token:
                args.token = get_token(args, self._config)
            if args.func:
                args.func(args)
        except SystemExit as e:
            self._exit_code.exit(e.code)
