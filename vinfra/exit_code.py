from applipy import Application


class ExitCode:

    def __init__(self, app: Application) -> None:
        self._app = app
        self._exit_code = 0

    def exit(self, code: int) -> None:
        self._exit_code = code

    @property
    def exit_code(self) -> int:
        return self._exit_code
