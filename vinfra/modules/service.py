import json
import sys

from enum import Enum

import requests

from applipy import Module

from .base import CliModule
from vinfra.common import CommonModule
from vinfra.completers import ResourceIdCompleter
from vinfra.config import Config


class Option(str, Enum):
    TASK = 'task'
    LOGS = 'logs'
    ENABLE = 'enable'
    DISABLE = 'disable'


class TaskActions(str, Enum):
    LIST = 'ls'
    GET = 'get'
    REMOVE = 'rm'


class ServiceCliModule(CliModule):

    APLICABLE_RESOURCE_TYPES = ['service', 'cronjob']

    def __init__(self, config: Config, resource_id_completer: ResourceIdCompleter):
        self.config = config
        self.resource_id_completer = resource_id_completer

    def list_tasks(self, args, cmd_resource_type):
        if cmd_resource_type not in self.APLICABLE_RESOURCE_TYPES:
            raise ValueError(cmd_resource_type)
        account_id, resource_type, resource_name = args.resource_id.split('-', 2)
        if cmd_resource_type != resource_type:
            raise ValueError(resource_type)

        response = requests.get(f'{args.api_endpoint}/v1/{resource_type}/tasks/{account_id}/{resource_name}',
                                headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), sys.stdout)
        self.config.add_known_account(account_id)

    def get_task(self, args, cmd_resource_type):
        if cmd_resource_type not in self.APLICABLE_RESOURCE_TYPES:
            raise ValueError(cmd_resource_type)
        account_id, resource_type, *resource_name_parts, task_id = args.task_id.split('-')
        resource_name = '-'.join(resource_name_parts)
        if cmd_resource_type != resource_type:
            raise ValueError(resource_type)

        response = requests.get(f'{args.api_endpoint}/v1/{resource_type}/tasks/{account_id}/{resource_name}/{task_id}',
                                headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), sys.stdout)
        self.config.add_known_account(account_id)

    def remove_task(self, args, cmd_resource_type):
        if cmd_resource_type not in self.APLICABLE_RESOURCE_TYPES:
            raise ValueError(cmd_resource_type)
        account_id, resource_type, *resource_name_parts, task_id = args.task_id.split('-')
        resource_name = '-'.join(resource_name_parts)
        if cmd_resource_type != resource_type:
            raise ValueError(resource_type)

        response = requests.delete(f'{args.api_endpoint}/v1/{resource_type}/tasks/{account_id}/{resource_name}/{task_id}',
                                   headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        print(response.text)
        self.config.add_known_account(account_id)

    def logs(self, args, cmd_resource_type):
        if cmd_resource_type not in self.APLICABLE_RESOURCE_TYPES:
            raise ValueError(cmd_resource_type)
        account_id, resource_type, resource_name = args.resource_id.split('-', 2)
        if cmd_resource_type != resource_type:
            raise ValueError(resource_type)

        with requests.get(f'{args.api_endpoint}/v1/{resource_type}/logs/{account_id}/{resource_name}',
                          headers={'Authorization': f'Bearer {args.token}'},
                          params={'n': args.tail},
                          stream=True) as response:
            if not response.ok:
                print(response.text)
                exit(2)
            try:
                for line in sorted(response.iter_lines()):
                    print(line.decode())
            except KeyboardInterrupt:
                ...
            finally:
                response.close()
        self.config.add_known_account(account_id)

    def handle_task(self, args, resource_type):
        task_action_mapping = {
            TaskActions.LIST: self.list_tasks,
            TaskActions.GET: self.get_task,
            TaskActions.REMOVE: self.remove_task,
        }
        return task_action_mapping[args.task_action](args, resource_type)

    def set_enable_cronjob(self, args, cmd_resource_type, enabled):
        if cmd_resource_type != 'cronjob':
            raise ValueError(cmd_resource_type)
        account_id, resource_type, resource_name = args.resource_id.split('-', 2)
        if cmd_resource_type != resource_type:
            raise ValueError(resource_type)
        response = requests.get(f'{args.api_endpoint}/v1/resources/{account_id}/{resource_type}/{resource_name}',
                                headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)

        resource_def = response.json()
        if resource_def.get('enabled') is not enabled:
            resource_def['enabled'] = enabled
            response = requests.put(f'{args.api_endpoint}/v1/resources/{account_id}/{resource_type}/{resource_name}',
                                     data=json.dumps(resource_def),
                                     headers={'Authorization': f'Bearer {args.token}'})
            if not response.ok:
                print(response.text)
                exit(2)
            resource_def = response.json()
        args.write_formatted(response.json(), sys.stdout)
        self.config.add_known_account(account_id)

    def register_parser(self, subparsers) -> None:
        service_parser = subparsers.add_parser('service', help='Query and manage the state of your existing services')
        cronjob_parser = subparsers.add_parser('cronjob', help='Query and manage the state of your existing cronjobs')

        option_mapping = {
            Option.TASK: self.handle_task,
            Option.LOGS: self.logs,
            Option.ENABLE: lambda args, resource_type: self.set_enable_cronjob(args, resource_type, True),
            Option.DISABLE: lambda args, resource_type: self.set_enable_cronjob(args, resource_type, False),
        }

        for resource_type, parser in [('service', service_parser), ('cronjob', cronjob_parser)]:
            parser.set_defaults(requires_token=True)

            subparsers = parser.add_subparsers()
            subparsers.required = True
            subparsers.dest = 'service_mode'

            if resource_type == 'cronjob':
                cronjob_enabled = subparsers.add_parser(Option.ENABLE.value, help='Set cronjob to enabled=true')
                cronjob_disabled = subparsers.add_parser(Option.DISABLE.value, help='Set cronjob to enabled=false')
                for p in (cronjob_enabled, cronjob_disabled):
                    p.add_argument('resource_id', help=f'Resource Id of the cronjob').completer = self.resource_id_completer


            parser_tasks = subparsers.add_parser(Option.TASK.value, help=f"Manage {resource_type}'s tasks")
            tasks_subparsers = parser_tasks.add_subparsers()
            tasks_subparsers.required = True
            tasks_subparsers.dest = 'task_action'
            parser_tasks_ls = tasks_subparsers.add_parser(
                TaskActions.LIST.value,
                help=f'List the tasks of a given {resource_type} and show their current state'
            )
            parser_tasks_get = tasks_subparsers.add_parser(
                TaskActions.GET.value,
                help='Get the task with the given task_id and show their current state'
            )
            parser_tasks_rm = tasks_subparsers.add_parser(
                TaskActions.REMOVE.value,
                help=('Remove the task with the given task_id. '
                      f'It will be replaced by a new {resource_type} replica. '
                      f'To scale down the {resource_type} update its resource definition.')
            )

            parser_logs = subparsers.add_parser(Option.LOGS.value, help=f'Print the logs of a {resource_type}')
            parser_logs.add_argument('-t', '--tail', default='10', help='Number of lines to print per replica. Defaults to 10')

            for p in (parser_tasks_ls, parser_logs):
                p.add_argument('resource_id', help=f'Resource Id of the {resource_type}').completer = self.resource_id_completer

            for p in (parser_tasks_get, parser_tasks_rm):
                p.add_argument('task_id', help='Full Id of the task: <resource_id>-<task_id>').completer = self.resource_id_completer

            parser.set_defaults(func=lambda args, resource_type=resource_type: option_mapping[args.service_mode](args, resource_type))


class ServiceModule(Module):

    def configure(self, bind, register):
        bind(CliModule, ServiceCliModule)

    @classmethod
    def depends_on(cls):
        return CommonModule,
