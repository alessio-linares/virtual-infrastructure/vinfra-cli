import json
import os
import sys

from argparse import ArgumentParser
from enum import Enum
from getpass import getpass

import yaml

from applipy import Module

from .base import CliModule
from vinfra.common import CommonModule
from vinfra.config import Config


class Option(str, Enum):
    TOKEN = 'identity'
    EDITOR = 'editor'

class TokenAction(str, Enum):
    ADD = 'add'
    SET = 'set'
    REMOVE = 'rm'
    DEFAULT = 'active'
    LIST = 'ls'


CONFIG_DIR = os.path.join(os.path.expanduser('~'), '.config', 'vinfra')
CONFIG_FILE = os.path.join(CONFIG_DIR, 'config')
TOKEN_HELP_TEXT = ("Manage your vinfra API tokens. This way you don't have "
                   "to pass it as parameter every time. Tokens will "
                   f"be stored raw in {CONFIG_FILE}")


class ConfigCliModule(CliModule):

    def __init__(self, config: Config):
        self.config = config

    def token(self, args):
        token = getpass(TOKEN_HELP_TEXT + '\nToken: ')
        self.config.token = token

    def token_add(self, args):
        token = getpass(TOKEN_HELP_TEXT + '\nToken: ')
        try:
            self.config.add_identity(args.identity_name, token)
        except ValueError:
            print('Error: identity already exists')
            exit(2)

        if len(self.config.list_identities()) == 1:
            token_default(args)

    def token_set(self, args):
        token = getpass(TOKEN_HELP_TEXT + '\nToken: ')
        try:
            self.config.set_identity(args.identity_name, token)
        except KeyError:
            print('Error: identity does not exist')
            exit(2)

    def token_remove(self, args):
        try:
            self.config.remove_identity(args.identity_name)
        except KeyError:
            ...

    def token_default(self, args):
        self.config.active_identity = args.identity_name
        print('Identity', args.identity_name, 'set as active')

    def token_list(self, args):
        for i in self.config.list_identities():
            print('*' if i == self.config.active_identity else ' ', i)

    def editor(self, args):
        self.config.editor = args.editor

    def register_parser(self, subparsers) -> None:
        parser = subparsers.add_parser('config', help='Configure your local vinfra client')

        subparsers = parser.add_subparsers()
        subparsers.required = True
        subparsers.dest = 'option'

        parser_token = subparsers.add_parser(Option.TOKEN.value, help=TOKEN_HELP_TEXT)
        parser_token.description = TOKEN_HELP_TEXT
        token_subparsers = parser_token.add_subparsers()
        token_subparsers.required = True
        token_subparsers.dest = 'token_action'
        parser_token_add = token_subparsers.add_parser(
            TokenAction.ADD.value,
            help='Add a new identity to the local identity store.'
        )
        parser_token_set = token_subparsers.add_parser(
            TokenAction.SET.value,
            help='Set the token for an identity to the local identity store.'
        )
        parser_token_rm = token_subparsers.add_parser(
            TokenAction.REMOVE.value,
            help='Remove an identity from the local identity store.'
        )
        parser_token_default = token_subparsers.add_parser(
            TokenAction.DEFAULT.value,
            help="Set the active identity to use. If not set you'll have to give the identity you want to use every time"
        )
        token_subparsers.add_parser(
            TokenAction.LIST.value,
            help='List all your locally stored identities'
        )

        parser_token_add.add_argument('identity_name', help='Name to give to your token to store it locally')
        for p in (parser_token_set, parser_token_rm, parser_token_default):
            p.add_argument('identity_name', help='Name of the identity', choices=self.config.list_identities(),
                           metavar='IDENTITY')

        parser_editor = subparsers.add_parser(Option.EDITOR.value, help='Set your text editor for vinfra')
        parser_editor.add_argument('editor', metavar='EDITOR_CMD', help='Your text editor command')

        token_action_mapping = {
            TokenAction.ADD: self.token_add,
            TokenAction.SET: self.token_set,
            TokenAction.REMOVE: self.token_remove,
            TokenAction.DEFAULT: self.token_default,
            TokenAction.LIST: self.token_list,
        }

        option_mapping = {
            Option.TOKEN: lambda args: token_action_mapping[args.token_action](args),
            Option.EDITOR: self.editor,
        }

        parser.set_defaults(func=lambda args: option_mapping[args.option](args))


class ConfigModule(Module):

    def configure(self, bind, register):
        bind(CliModule, ConfigCliModule)

    @classmethod
    def depends_on(cls):
        return CommonModule,
