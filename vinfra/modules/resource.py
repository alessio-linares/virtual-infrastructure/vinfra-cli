import difflib
import json
import os
import sys
import tempfile

from enum import Enum
from subprocess import call

import requests
import yaml

from applipy import Module

from .base import CliModule
from vinfra.common import CommonModule
from vinfra.completers import AccountCompleter, ResourceTypeCompleter, ResourceIdCompleter
from vinfra.config import Config


SCHEMA_DEF_HELP = (
    'The resource definition file is a yaml file following the schema of the resource. '
    'You can get the schema of the resource by running `vinfra resource schema <resource_type>`.'
)


class Action(str, Enum):
    CREATE = 'create'
    REMOVE = 'rm'
    UPDATE = 'update'
    GET = 'get'
    LIST = 'ls'
    SCHEMA = 'schema'
    DIFF = 'diff'
    EDIT = 'edit'


class ResourceCliModule(CliModule):

    def __init__(self,
                 config: Config,
                 account_completer: AccountCompleter,
                 resource_type_completer: ResourceTypeCompleter,
                 resource_id_completer: ResourceIdCompleter):
        self.config = config
        self.account_completer = account_completer
        self.resource_type_completer = resource_type_completer
        self.resource_id_completer = resource_id_completer

    def list(self, args):
        url = f'{args.api_endpoint}/v1/resources/{args.account}'
        if args.resource_type:
            url += f'/{args.resource_type}'
        response = requests.get(url, headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), sys.stdout)
        self.config.add_known_account(args.account)

    def get(self, args, outfile=sys.stdout):
        path = '/'.join(args.resource_id.split('-', 2))
        response = requests.get(f'{args.api_endpoint}/v1/resources/{path}',
                                headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), outfile)
        self.config.add_known_account(args.resource_id.split('-', 1)[0])

    def create(self, args):
        if args.file == '-':
            resource_def = yaml.safe_load(sys.stdin)
        else:
            with open(args.file, 'rb') as f:
                resource_def = yaml.safe_load(f)

        account = resource_def['account_id']
        type = resource_def['type']
        name = resource_def['name']
        response = requests.post(f'{args.api_endpoint}/v1/resources/{account}/{type}/{name}',
                                 data=json.dumps(resource_def),
                                 headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), sys.stdout)
        self.config.add_known_account(account)

    def update(self, args):
        if args.file == '-':
            resource_def = yaml.safe_load(sys.stdin)
        else:
            with open(args.file, 'rb') as f:
                resource_def = yaml.safe_load(f)

        account = resource_def['account_id']
        type = resource_def['type']
        name = resource_def['name']
        response = requests.put(f'{args.api_endpoint}/v1/resources/{account}/{type}/{name}',
                                 data=json.dumps(resource_def),
                                 headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), sys.stdout)
        self.config.add_known_account(account)

    def remove(self, args):
        path = '/'.join(args.resource_id.split('-', 2))
        response = requests.delete(f'{args.api_endpoint}/v1/resources/{path}',
                                   headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        print(response.text)
        self.config.add_known_account(args.resource_id.split('-', 1)[0])

    def schema(self, args):
        response = requests.get(f'{args.api_endpoint}/v1/schema/{args.resource_type}',
                                   headers={'Authorization': f'Bearer {args.token}'})
        if not response.ok:
            print(response.text)
            exit(2)
        args.write_formatted(response.json(), sys.stdout)

    def diff(self, args):
        if args.file == '-':
            resource_def = yaml.safe_load(sys.stdin)
            args.file = sys.stdin.name
        else:
            with open(args.file, 'rb') as f:
                resource_def = yaml.safe_load(f)

        account = resource_def['account_id']
        type = resource_def['type']
        name = resource_def['name']
        args.resource_id = '-'.join((str(account), type, name))
        with tempfile.TemporaryFile(mode='w+') as a, tempfile.TemporaryFile(mode='w+') as b:
            self.get(args, outfile=a)
            args.write_formatted(resource_def, b)
            a.seek(0)
            b.seek(0)
            sys.stdout.writelines(difflib.unified_diff(a.readlines(), b.readlines(),
                                                       fromfile=args.resource_id + '.yaml', tofile=args.file))

    def edit(self, args):
        if not self.config.editor:
            print('No editor configured. Run `vinfra config editor <your_editor>` to set an editor for vinfra.')
            exit(1)
        with tempfile.TemporaryDirectory() as dirname:
            filename = os.path.join(dirname, args.resource_id + '.yaml')
            with open(filename, 'w') as f:
                self.get(args, outfile=f)
            retcode = call([self.config.editor, filename])
            if retcode == 0:
                args.file = filename
                self.update(args)
            else:
                print('Update cancelled')

    def register_parser(self, subparsers) -> None:
        parser = subparsers.add_parser('resource', help='Manage your resources')
        parser.set_defaults(requires_token=True)

        subparsers = parser.add_subparsers(title='actions')
        subparsers.required = True
        subparsers.dest = 'action'
        parser_create = subparsers.add_parser(Action.CREATE.value, help='Create resource')
        parser_create.description = (
            'Create a new vinfra resource from a resource definition file. '
            + SCHEMA_DEF_HELP
        )
        parser_update = subparsers.add_parser(Action.UPDATE.value, help='Update resource')
        parser_update.description = (
            'Update an existing vinfra resource using a resource definition file. '
            + SCHEMA_DEF_HELP
        )
        parser_remove = subparsers.add_parser(Action.REMOVE.value, help='Remove resource')
        parser_remove.description = 'Update an existing vinfra resource by resource id.'
        parser_get = subparsers.add_parser(Action.GET.value, help='Get resource')
        parser_get.description = (
            "Retrieve an existing vinfra resource's definition by resource id. "
            'If --format=yaml (the default behavior) the output is a valid resource definition file. '
            + SCHEMA_DEF_HELP
        )
        parser_list = subparsers.add_parser(Action.LIST.value, help='List resources in account')
        parser_schema = subparsers.add_parser(Action.SCHEMA.value, help='Print schema for resource type')
        parser_diff = subparsers.add_parser(Action.DIFF.value,
                                            help='Print diff between vinfra state and a vinfra resource definition file')
        parser_edit = subparsers.add_parser(Action.EDIT.value,
                                            help='Edit an existing vinfra resource')
        parser_edit.description = (
            'Open your text editor with current vinfra resource state and update it with the changes. '
            'Editor can be configured with `vinfra config editor <your_editor>`. If not configured, $EDITOR will be used'
        )

        for p in (parser_create, parser_update, parser_diff):
            p.add_argument('-f', '--file', type=str, required=True,
                           help='Resource definition file. If value is `-` then stdin is used.')

        for p in (parser_get, parser_remove, parser_edit):
            p.add_argument('resource_id', type=str, help='Resource Id').completer = self.resource_id_completer

        parser_list.add_argument('account', type=int).completer = self.account_completer
        parser_list.add_argument('resource_type', default='', nargs='?', type=str).completer = self.resource_type_completer

        parser_schema.add_argument('resource_type', type=str).completer = self.resource_type_completer

        action_mapping = {
            Action.LIST: self.list,
            Action.GET: self.get,
            Action.CREATE: self.create,
            Action.UPDATE: self.update,
            Action.REMOVE: self.remove,
            Action.SCHEMA: self.schema,
            Action.DIFF: self.diff,
            Action.EDIT: self.edit,
        }
        parser.set_defaults(func=lambda args: action_mapping[args.action](args))


class ResourceModule(Module):

    def configure(self, bind, register):
        bind(CliModule, ResourceCliModule)

    @classmethod
    def depends_on(cls):
        return CommonModule,
